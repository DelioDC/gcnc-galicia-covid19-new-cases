#!/usr/bin/bash
################################################################################
#                       GCNC - Galicia covid19 new cases
################################################################################
# Version: 20210830
# Author: deliodc (https://gitlab.com/DelioDC)
# ------------------------------------------------------------------------------
# Description: Request a JSON with the galician towns coronavirus new cases in
#   last 14 days data to the official Sergas website:
#     https://coronavirus.sergas.gal/datos/
#
#   Parses the CSV data embed in the requested JSON, gets data and prints a new
#   clear JSON with all towns data or one town data.
#
# ! This is a unofficial script, use only for test purposes.
#
################################################################################
# License:
################################################################################
# Copyright 2020 Delio Docampo Cordeiro
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################
# Params:
#
#   -h, --help Prints the help.
#
#   -a, --all-towns-json
#       Returns a json with all towns data.
#
#   -i, --json-id-list
#       Returns a json with all towns "name":"id" data list.
#
#   --json-by-town-name="town_name_string"
#       Returns a json with objects which name contains town_name_string string.
#
#   --json-by-town-id-id="id_number(11 characteres)"
#       Returns a json with data of the town selected by id.
#
################################################################################
#
# Colors for echo -e:
#

E_ERROR='\e[1;31m'   # Bold, red
E_INFO='\e[0;34m'    # Blue
E_WORK='\e[0;32m'    # Green
E_NC='\033[0m'       # No color

#
# Connection vars:
#

URL_ORIGINAL_DATA='http://datawrapper.dwcdn.net/jKpTc'
USER_AGENT='Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
OFFICIAL_SOURCE='https://coronavirus.sergas.gal/datos/' # To provide the official source information.

#
# Library and commands vars:
#

SCRIPT_BASE_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
LIB_PATH="$SCRIPT_BASE_PATH/lib"

declare -A LIBRARY_FILES
LIBRARY_FILES[awk_csv]="$LIB_PATH/awk/csv_galicia_new_cases_to_json.awk"
LIBRARY_FILES[bash_scripts_toolbox]="$LIB_PATH/bash/bash_scripts_toolbox.sh"

# Load bash libraries:
if [[ -r "${LIBRARY_FILES[bash_scripts_toolbox]}" ]]; then
    . ${LIBRARY_FILES[bash_scripts_toolbox]}
else
    >&2 echo -e  $E_ERROR"ERROR: lib file [${LIBRARY_FILES[bash_scripts_toolbox]}] not accessible, if exists check read permissions"$E_NC
    exit
fi

#
# Set script required commands:
#

REQUIRED_COMMANDS=(jq curl gawk sed grep cut date)

#
# functions set_ :
#

function set_json() {
    # Description:
    #   Defines JSON variable with a paresed json data.
    #######################################################
    local today_url_original_data=$(get_redirection_url "$URL_ORIGINAL_DATA")
    local today_raw_data=$(curl_call "$today_url_original_data")

    if [[ ! -z $today_raw_data ]]; then
        JSON=$(echo -e "$today_raw_data"                |
               grep -ao 'JSON.parse(".*")'              |
               sed -r 's/JSON\.parse[(]"(.*)"[)]/\1/g'  |
               sed -r 's/\\"/"/g'                       |
              jq '. | { publishedAt: .chart.publishedAt, lastModifiedAt: .chart.lastModifiedAt}'
              )
    else
        exit_with_error_message "set_json \$today_raw_data is empty or undefined"
    fi
}

function set_csv () {
    # Description:
    #   Calls set_json(), parses csv data embed in JSON,
    #    and sets CSV global variable.
    #######################################################
    set_json
    local today_url_original_data=$(get_redirection_url "$URL_ORIGINAL_DATA")
    local temp_url=$today_url_original_data"dataset.csv"
    CSV=$(curl_call "$temp_url")

    JSON=$(echo "$JSON" | jq --arg CSV  "$CSV" '. + {csv : $CSV}')
}

function set_published_at_date () {
    # Description:
    #   Sets PUBLISHED_AT_DATE variable with the
    #   JSON publishedAt data attrib.
    ####################################
    if [[ -z $JSON ]];then
        set_json
    fi
    if [[ ! -z $JSON ]];then
        if [[ -z $PUBLISHED_AT_DATE && ! -z $JSON ]]; then
            PUBLISHED_AT_DATE=$(echo "$JSON" | jq -r '.publishedAt' )
        fi
    else
        exit_with_error_message "set_published_at_date \$JSON is empty or undefined"
    fi
}

function set_last_modified_at_date () {
    # Description:
    #   Sets LAST_MODIFIED_AT_DATE variable with the
    #   JSON lastModifiedAt data attrib.
    ####################################
    if [[ -z $JSON ]];then
        set_json
    fi
    if [[ ! -z $JSON ]];then
        if [[ -z $LAST_MODIFIED_AT_DATE && ! -z $JSON ]]; then
            LAST_MODIFIED_AT_DATE=$(echo "$JSON" | jq -r '.lastModifiedAt' )
        fi
    else
        exit_with_error_message "set_last_modified_at_date \$JSON is empty or undefined"
    fi
}

function set_json_galicia() {
    # Description:
    #   Sets the JSON_GALICIA var content.
    #   With the parsed by awk CSV content.
    ########################################
    set_csv
    if [[ -z $PUBLISHED_AT_DATE ]]; then
        set_published_at_date
    fi
    if [[ -z $LAST_MODIFIED_AT_DATE ]]; then
        set_last_modified_at_date
    fi

    if [[ ! -z "$CSV" && ! -z $PUBLISHED_AT_DATE && ! -z $LAST_MODIFIED_AT_DATE ]]; then
        JSON_GALICIA=$(parse_csv_with_awk_script "$CSV" "${LIBRARY_FILES[awk_csv]}" |
                        jq --arg PUBLISHED_AT_DATE $PUBLISHED_AT_DATE            \
                           --arg LAST_MODIFIED_AT_DATE $LAST_MODIFIED_AT_DATE    \
                           --arg official_source $OFFICIAL_SOURCE                \
                           ' {publishedAt: $PUBLISHED_AT_DATE, lastModifiedAtDate: $LAST_MODIFIED_AT_DATE, official_source: $official_source} + .'
           )
    else
        exit_with_error_message "set_json_galicia \$CSV, \$PUBLISHED_AT_DATE or \$LAST_MODIFIED_AT_DATE are not defined"
    fi
}
#
# functions CSC print_:
#

function print_all_towns_csv () {
    # Description:
    #   Prints a CSV which contains all towns
    #   restrictions.
    ############################################
    if [[  -z $CSV ]]; then
        set_csv
    fi
    if [[ ! -z $CSV ]]; then
        echo "$CSV"
    else
        exit_with_error_message "print_all_towns_csv \$CSV is not defined"
    fi
}


#
# functions JSON print_:
#

function print_all_towns_json () {
    # Description:
    #   Prints a JSON with publishedAt attrib and a array
    #   which contains objects of all towns.
    ############################################
    if [[  -z $JSON_GALICIA ]]; then
        set_json_galicia
    fi
    if [[ ! -z $JSON_GALICIA ]]; then
        echo -e "$JSON_GALICIA"
    else
        exit_with_error_message "print_all_towns_json \$JSON_GALICIA is not defined"
    fi
}

function print_json_id_list {
    # Description:
    #   Returns JSON with objects of all towns, which only
    #   contains name and id attribs sorted by town name.
    ####################################
    if [[ -z $JSON_GALICIA ]]; then
        set_json_galicia
    fi
    if [[ ! -z $JSON_GALICIA ]]; then
        # Create a json with a array sorted by suboject attrib name, and builds objects who only contains 2 fields "name" and "id".
        local id_raw_list=$(echo -e "$JSON_GALICIA" | jq '[.town | sort_by(.name) | .[] | {"name": .name, "id": .id}  ]')
        echo -e "$id_raw_list"
    else
        exit_with_error_message "print_json_id_list \$JSON_GALICIA is empty or undefined"
    fi
}

function print_json_town_by_id {
    # Description:
    #   Returns a JSON object with the data of the town
    #   which id attrib is equal to passed id, and
    #   appends publishedAt attrib.
    # Params:
    #   - Id of desired town
    #########################################################
    if [[ -z $JSON_GALICIA ]]; then
        set_json_galicia
    fi
    if [[ ! -z $JSON_GALICIA && ! -z $1 ]]; then
        local id_raw_list=$(echo -e "$JSON_GALICIA" |
        jq --arg ID "$1" '.publishedAt as $publishedAt | .lastModifiedAtDate as $lastModifiedAtDate | .official_source as $official_source | .town[] | select(.id==$ID)  | . + {publishedAt: $publishedAt} + {lastModifiedAtDate: $lastModifiedAtDate} + {official_source: $official_source}')
        echo -e "$id_raw_list"
    else
        exit_with_error_message "print_json_town_by_id \$JSON_GALICIA or \$1 (id) are not defined"
    fi
}

function print_json_town_by_name {
    # Description:
    #  Returns a JSON object with the data of the town
    #  or towns which name contains the string, and
    #  appends publishedAt attrib.
    # Params:
    #   - String to search in name attrib
    #########################################################
    if [[ -z $JSON_GALICIA ]]; then
        set_json_galicia
    fi
    if [[ ! -z $JSON_GALICIA && ! -z $1 ]]; then
        local regexp='.*'"$1"'.*'
        # jq test makes a regular expresion search without case sensitive (;"i")
        local id_raw_list=$(echo -e "$JSON_GALICIA" | jq --arg regexp "$regexp" '.publishedAt as $publishedAt | .lastModifiedAtDate as $lastModifiedAtDate | .official_source as $official_source | .town[] | select(.name|test($regexp;"i"))  | . + {publishedAt: $publishedAt} + {official_source: $official_source}')
        echo -e "$id_raw_list"
    else
        exit_with_error_message "print_json_town_by_name \$JSON_GALICIA or \$1 (name) are not defined"
    fi
}

#
# Print messages
#

function print_message_town_by_id {
    # Description:
    #   Formats the town JSON information to human readable
    #   and prints the result.
    # Params:
    #   - Town ID
    ########################################################
    if [[ ! -z $1 ]]; then
        local json_town_data=$(print_json_town_by_id $1)
        if [[ ! -z $json_town_data ]] ; then
            local town_name=$(echo -e             "$json_town_data" | jq -r .name )
            local town_cases_at_14_days=$(echo -e        "$json_town_data" | jq -r .cases_at_14_days )
            local town_incidence_at_14_days=$(echo -e "$json_town_data" | jq -r .incidence_at_14_days )
            local town_cases_at_7_days=$(echo -e        "$json_town_data" | jq -r .cases_at_7_days )
            local town_incidence_at_7_days=$(echo -e "$json_town_data" | jq -r .incidence_at_7_days )
            local town_publishedAtTime=$(date -d"$(echo -e "$json_town_data" | jq -r .publishedAt )" +"%H:%M%P")
            local town_publishedAtDate=$(date -d"$(echo -e "$json_town_data" | jq -r .publishedAt )" +"%Y-%m-%d")
            local town_official_source=$(echo -e  "$json_town_data" | jq -r .official_source )

            # Change name spaces by underlines:
            town_name="$(echo -e "$town_name" | sed -r 's/ /_/g')"

    # ----------------------------
            printf "🦠#COVID19_casos_%s
Últimos 7 días (%s %s):
    Casos novos:  %s
    Incidencia acumulada: %s
Últimos 14 días (%s %s):
    Casos novos:  %s
    Incidencia acumulada: %s
+info na fonte oficial:
%s\n" "$town_name" "$town_publishedAtDate" "$town_publishedAtTime" "$town_cases_at_7_days" "$town_incidence_at_7_days" "$town_publishedAtDate" "$town_publishedAtTime" "$town_cases_at_14_days" "$town_incidence_at_14_days" "$town_official_source"
    # ----------------------------

        else
            exit_with_error_message "print_message_town_by_id json_town_data not defined"
        fi
    else
        exit_with_error_message "print_message_town_by_id need at least 1 parameter"
    fi
}

#
# functions help, documentation:
#

function print_help {
    # Description:
    #   Prints the full help for user.
    #######################################
cat <<EOF
SYNOPSIS
    ./galicia-covid19-new-cases.sh <option>

DESCRIPTION:
    Request a JSON with the galician town coronavirus new cases in last 14 days
    data to the official Sergas website:
        https://coronavirus.sergas.gal/datos/

    Parses the CSV data embed in the requested JSON, gets data and prints a new
    clear JSON with all towns data or one town data.

OPTIONS:
    -h, --help Prints the help.

    -a, --all-towns-json
            Returns a json with all towns data.

    -A, --all-towns-csv
            Returns a csv with all towns data.

    -i, --json-id-list
            Returns a json with all towns "name":"id" data list.

    --json-by-town-name="town_name_string"
            Returns a json with objects which name contains town_name_string
            string.

    --json-by-town-id="id_number(11 characteres)"
            Returns a json with data of the town selected by id.

    --message-by-town-id="id_number(11 characteres)"
            Formats the town JSON information to human readable and prints the
            result.

EXAMPLES:
  Print JSON with only all towns id and name:
    ./galicia-covid19-new-cases.sh -i
    RESULT:
        [
            ... rest of json objects ...
            {
                "name": "Vigo",
                "id": "34123636057"
            },
            ... rest of json objects ...
        ]
  Print JSON of town name who contains "vig":
    ./galicia-covid19-new-cases.sh --json-by-town-name="Vig"
    RESULT:
        {
            "id": "34123636057",
            "name": "Vigo",
            "cases_at_14_days": "<new_cases>",
            "incidence_at_14_days": "<incidence>",
            "cases_at_7_days": "<new_cases>",
            "incidence_at_7_days": "<incidence>",
            "publishedAt": "<datetime_in_utc>",
            "official_source": "<url_to_official_source>"
        }

  Print JSON of town which id is 34123636057:
    ./this_script.sh --json-by-town-id=34123636057
    RESULT:
        (the same JSON object as in the example above)
EOF
}

#
# Check's:
#

# Check required commands:
for binary in ${REQUIRED_COMMANDS[@]}
do
    if ! command -v $binary &> /dev/null ; then
        exit_with_error_message "Binary [$binary] not found or is not installed in system"
    fi
done

# Check required lib dir:
if [[ ! -d $LIB_PATH ]]; then
        exit_with_error_message "Cannot define \$LIB_PATH as [$LIB_PATH] directory not found"
fi

# Check required library files:
for library_file in ${LIBRARY_FILES[@]}
do
    if [[ ! -f "$library_file" ]]; then
        exit_with_error_message "Library file [$library_file] not found"
    fi
done

# CHECK SCRIPT PARAMS:
if [ ! -z $1 ] && [ -z $2 ]; then
    TEMP_REGEXP="(^--all-towns-json$)|(^-a$)"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        print_all_towns_json
        PARAM_FINDED="yes"
    fi
    TEMP_REGEXP="(^--all-towns-csv$)|(^-A$)"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        print_all_towns_csv
        PARAM_FINDED="yes"
    fi
    TEMP_REGEXP="(^--json-id-list$)|(^-i$)"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        print_json_id_list
        PARAM_FINDED="yes"
    fi
    TEMP_REGEXP="^--json-by-town-id=[0-9]{11,11}$"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        TOWN_ID=$(echo  "$1" | grep -oP '[0-9]{11,11}')
        print_json_town_by_id $TOWN_ID
        PARAM_FINDED="yes"
    fi
    TEMP_REGEXP="^--json-by-town-name=[\w]*"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        TOWN_NAME=$(echo  "$1" | cut -d"=" -f2-)
        print_json_town_by_name "$TOWN_NAME"
        PARAM_FINDED="yes"
    fi

    TEMP_REGEXP="^--message-by-town-id=[0-9]{11,11}$"
    if [[ $1 =~ $TEMP_REGEXP ]]; then
        TOWN_ID=$(echo  "$1" | grep -oP '[0-9]{11,11}')
        print_message_town_by_id $TOWN_ID
        PARAM_FINDED="yes"
    fi

    TEMP_REGEXP="(^--help$)|(^-h$)"
    if [[ $1 =~ $TEMP_REGEXP ]] ; then
        print_help
        PARAM_FINDED="yes"
    fi

    if [[ -z $PARAM_FINDED ]]; then
        exit_with_error_message "Check script call parameters, parameter not recognized"
    fi
    unset -v TEMP_REGEXP
else
    print_help
    exit_with_error_message "Check script call parameters"
fi
