#!/usr/bin/gawk
################################################################################
# Version: 20210830
# Author: deliodc (https://gitlab.com/DelioDC)
################################################################################
# License:
################################################################################
# Copyright 2020 Delio Docampo Cordeiro
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################
# Records:
# RS  - Record Separator
# ORS - Output Record Separator
# NR  - total Number of Records or Line Number
# FNR - Number of Records relative to the current input file
# ------------------------------------------------------------------------------
# Fields:
# FS  - Field Separator
# OFS - Output Field Separator
# NF  - Number of Fields in a record
# ------------------------------------------------------------------------------
# FILENAME - Name of input file.
################################################################################
# record_1(field1,field2,field3)
# record_2(field1,field2,field3)
# record_3(field1,field2,field3)
################################################################################
# https://www.gnu.org/software/gawk/manual/html_node/index.html#SEC_Contents
################################################################################
function clean_surrounding_quotes (string) {
    # Returns the string with out start and end '\\"'
    return gensub(/"(.*)"/, "\\1", "g", string);
}
function to_capitalizate (string) {
    # Returns string with first character uppercase and rest lowercase
    return toupper(substr(string,1,1)) tolower(substr(string,2));
}
function br_to_newline (string) {
    # Returns the string with the "<br>" replaced by "\n":
    string = gensub(/<br>/, "\\\\n", "g", string);
    # Needed to replace "\n \n" substrings:
    string = gensub( "\\\\n\\\\n", "\\\\n" , "g", string);
    # Adds a dash and 1 space at the start of lines:
    string = gensub( "\\\\n", "\\\\n- " , "g", string);
    return string;
}
function remove_lines_with_a_entities (string) {
    # Remove lines like:
    # \n- <a href='https://coronavirus.sergas.gal/Contidos/Restricions-Nivel-Medio-Alto'>[+info]</a>
    string = gensub(/\\n- <a[ '/>\[\]\-a-zA-Z0-9@:%._\+~#=]*<\/a>/, "", "g", string);
    return string;
}
function clean_description (string) {
    string = br_to_newline(string);
    string = remove_lines_with_a_entities(string);
    return string;
}
function get_cases(string) {
    string = clean_description(string);
    if( string ~ /(.*: )([0-9\.]+)(.*)/){
        #"Número de novos casos diagnosticados no concello: 2.477."
        string = gensub(/(.*: )([0-9]+[\.]?[0-9]+)(.*)/,"\\2","g",string);
    }else if( string ~ /(.*: )(entre)(.*)/){
        #"Número de novos casos diagnosticados no concello: entre 1 e 9."
        string = gensub(/(.*: )entre ([0-9]+[\.]?[0-9]+) e ([0-9]+[\.]?[0-9]+)(.*)/,"entre \\2 e \\3","g",string);
    }else{
        # "Sen novos casos diagnosticados no concello."
        string = 0;
    }
    return string;
}
function get_incidence(string) {
    string = clean_description(string);
    # "Incidencia acumulada para o concello: >150 e ≤250.",
    # "Incidencia acumulada para o concello: ≤25.",
    # "Incidencia acumulada para o concello: >25 e ≤50.",
    # "Incidencia acumulada para o concello: >250 e ≤500.",
    # "Incidencia acumulada para o concello: >50 e ≤150.",
    # "Incidencia acumulada para o concello: >500.",
    if( string ~ /(.*: )([>≥<≤].*) e ([>≥<≤].*)\./){
        #"Número de novos casos diagnosticados no concello: 40."
        string = gensub(/(.*: )([>≥<≤].*) e ([>≥<≤].*)\./,"\\2 e \\3","g",string);
    }else if( string ~ /(.*: )([>≥<≤].*)\./){
        #"Número de novos casos diagnosticados no concello: 40."
        string = gensub(/(.*: )([>≥<≤].*)\./,"\\2","g",string);
    }
    return string;
}
function get_string_between_colon_and_dot (string) {
    return  gensub(/.*: (.*)\./, "\\1", "g", string);
}
BEGIN{
    # Field separator:
    FS=",";
    # New csv format uses the default record separator, this is don't needed:
    # Record separator:
    #RS=";\n";
    EXPECTED_RECODS=313; #313 Galicia towns.

    #
    # Print begin of JSON:
    #
    printf "{\"town\":[\n";
}
# For every record:
{
    #
    # Define column numbers:
    #
    if (NR == 1) {
        for (i=1; i<=NF; i++) {
            #
            # i = field number
            # $i = field content
            #
            $i = clean_surrounding_quotes($i);

            #ID,NOME,CASOS_14_DIAS,NIVEL_14_DIAS,CASOS_7_DIAS,NIVEL_7_DIAS,COR;
            if ( $i ~ /^ID$/ ) {
                COL_ID=i;
            }
            if ( $i ~ /^NOME$/ ) {
                COL_NAME=i;
            }
            if ( $i ~ /^CASOS_14_DIAS$/ ) {
                COL_CASES_14_DAYS=i;
            }
            if ( $i ~ /^NIVEL_14_DIAS$/ ) {
                COL_INCIDENCE_14_DAYS=i;
            }

            if ( $i ~ /^CASOS_7_DIAS$/ ) {
                COL_CASES_7_DAYS=i;
            }
            if ( $i ~ /^NIVEL_7_DIAS$/ ) {
                COL_INCIDENCE_7_DAYS=i;
            }
        }
        next; # Jumps to next record.
    }

    #
    # clean surrounding quotes:
    #

    $COL_ID=clean_surrounding_quotes($COL_ID);
    $COL_NAME=clean_surrounding_quotes($COL_NAME);

    $COL_CASES_14_DAYS=clean_surrounding_quotes($COL_CASES_14_DAYS);
    $COL_INCIDENCE_14_DAYS=clean_surrounding_quotes($COL_INCIDENCE_14_DAYS);

    $COL_CASES_7_DAYS=clean_surrounding_quotes($COL_CASES_7_DAYS);
    $COL_INCIDENCE_7_DAYS=clean_surrounding_quotes($COL_INCIDENCE_7_DAYS);

    #
    # Set raw variables:
    #

    if ( $COL_ID ~ /^[0-9]{11,11}$/ ) {
        id_raw=$COL_ID;
    }
    if ( $COL_NAME ~ /^[a-zA-Z áéíóúÁÉÍÓÚñÑ\(\)\-]{1,33}$/ ) {
        # "CORUÑA (A)"
        name_raw=$COL_NAME;
    }

    if ( $COL_CASES_14_DAYS ~ //){
        cases_at_14_days_raw=$COL_CASES_14_DAYS;
    }
    if ( $COL_INCIDENCE_14_DAYS ~ //){
        incidence_at_14_days_raw=$COL_INCIDENCE_14_DAYS;
    }

    if ( $COL_CASES_7_DAYS ~ //){
        cases_at_7_days_raw=$COL_CASES_7_DAYS;
    }
    if ( $COL_INCIDENCE_7_DAYS ~ //){
        incidence_at_7_days_raw=$COL_INCIDENCE_7_DAYS;
    }


    #
    # Cleaned variables:
    #

    id = id_raw;
    name = to_capitalizate(name_raw);

    cases_at_14_days=get_cases(cases_at_14_days_raw);
    incidence_at_14_days=get_incidence(incidence_at_14_days_raw);

    cases_at_7_days=get_cases(cases_at_7_days_raw);
    incidence_at_7_days=get_incidence(incidence_at_7_days_raw);

    #
    # Print JSON object:
    #

    printf "\t{\n";
    printf "\t\t\"id\":\"%s\",\n", id;
    printf "\t\t\"name\":\"%s\",\n", name;
    printf "\t\t\"cases_at_14_days\":\"%s\",\n", cases_at_14_days;
    printf "\t\t\"incidence_at_14_days\":\"%s\",\n", incidence_at_14_days;
    printf "\t\t\"cases_at_7_days\":\"%s\",\n", cases_at_7_days;
    printf "\t\t\"incidence_at_7_days\":\"%s\"\n", incidence_at_7_days;
    printf "\t}";
    if ( (NR - 1) < EXPECTED_RECODS ) {
        printf ",";
    }
    printf "\n";

}
END{
    #
    # Print end of JSON:
    #

    printf "]}\n";
}
