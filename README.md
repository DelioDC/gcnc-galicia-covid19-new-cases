# GCNC - Galicia covid19 new cases

Parses official info published in [coronavirus.sergas.gal](https://coronavirus.sergas.gal/datos/#/gl-ES/galicia),
and returns galician towns coronavirus new cases in last 14 days and 7 days.

_*This script is unofficial, and we don't provide any guarantees of the provided data is correct, only use for test purposes._

## Description

Request a JSON with the galician towns coronavirus new cases in last 14 days and 7 days data to the official Sergas website:

https://coronavirus.sergas.gal/datos/

Parses the CSV data embed in the requested JSON, proceses data and prints a new JSON with all towns data or one town data or a list of towns ids.

### Licensing
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.

## Required dependences

| Name: | Tested with version: | Website:                                                                            |
| :---: | :---:                | :---                                                                                |
| curl  | 7.73.0               | [https://curl.haxx.se](https://curl.haxx.se)                                        |
| jq    | jq-1.6               | [https://github.com/stedolan/jq](https://github.com/stedolan/jq/)                   |
| gawk  | 5.1.0                | [https://www.gnu.org/software/gawk/](https://www.gnu.org/software/gawk/)            |
| grep  | 3.6                  | [https://www.gnu.org/software/grep/](https://www.gnu.org/software/grep/)            |
| sed   | 4.8                  | [https://www.gnu.org/software/sed/](https://www.gnu.org/software/sed/)              |
| cut   | 8.32                 | [https://www.gnu.org/software/coreutils/]([https://www.gnu.org/software/coreutils/) |
| date  | 8.32                 | [https://www.gnu.org/software/coreutils/]([https://www.gnu.org/software/coreutils/) |

## Usage

Usage:

```
./galicia-covid19-new-cases.sh [PARAMETER]
```

|Parameter:                                             |Description:                                                                |
|:---                                                   | :---                                                                       |
| -a, --all-towns-json                                  | Returns a json with all towns data.                                        |
| -A, --all-towns-csv                                   | Returns a csv with all towns data.                                         |
| -i, --json-id-list                                    | Returns a json with all towns "name":"id" data list.                       |
| --json-by-town-name="town_name_string"                | Returns a json with objects which name contains town_name_string string.   |
| --json-by-town-id="id_number(11 characteres)"         | Returns a json with data of the town selected by id.                       |
| --message-by-town-id="id_number(11 characteres)"      | Formats the town JSON information to human readable and prints the result. |
| -h, --help                                            | Prints the help.                                                           |


### Examples

#### Print a JSON with all towns data:

```
    ./galicia-covid19-new-cases.sh --all-towns-json
```
**RESULT:**

```
{
"publishedAt": "<datetime_in_utc>",
"official_source": "<url_to_official_source>",
"town": [
    {
    "id": "34121515001",
    "name": "Abegondo",
    "cases_at_14_days": "<new_cases>",
    "incidence_at_14_days": "<incidence>",
    "cases_at_7_days": "<new_cases>",
    "incidence_at_7_days": "<incidence>"
    },
    {
    "id": "34121515002",
    "name": "Ames",
    "cases_at_14_days": "<new_cases>",
    "incidence_at_14_days": "<incidence>",
    "cases_at_7_days": "<new_cases>",
    "incidence_at_7_days": "<incidence>"
    },
    ... rest of json objects ...
    {
    "id": "34123636902",
    "name": "Cerdedo-cotobade",
    "cases_at_14_days": "<new_cases>",
    "incidence_at_14_days": "<incidence>",
    "cases_at_7_days": "<new_cases>",
    "incidence_at_7_days": "<incidence>"
    }
]
}
```

#### Print JSON with only all towns id and name:

```
./galicia-covid19-new-cases.sh -i
```
**RESULT:**

```
    [
        ... rest of json objects ...
        {
            "name": "Vigo",
            "id": "34123636057"
        },
        ... rest of json objects ...
    ]
```

#### Print JSON of town name who contains "vig":

```
./galicia-covid19-new-cases.sh --json-by-town-name="Vig"
```
**RESULT:**

```
    {
        "id": "34123636057",
        "name": "Vigo",
        "cases_at_14_days": "<new_cases>",
        "incidence_at_14_days": "<incidence>",
        "cases_at_7_days": "<new_cases>",
        "incidence_at_7_days": "<incidence>",
        "publishedAt": "<datetime_in_utc>",
        "official_source": "<url_to_official_source>"
    }
```
#### Print JSON of town which id is 34123636057:

```
./galicia-covid19-new-cases.sh --json-by-town-id=34123636057
```
**RESULT:**

```
    {
        "id": "34123636057",
        "name": "Vigo",
        "cases_at_14_days": "<new_cases>",
        "incidence_at_14_days": "<incidence>",
        "cases_at_7_days": "<new_cases>",
        "incidence_at_7_days": "<incidence>",
        "publishedAt": "<datetime_in_utc>",
        "official_source": "<url_to_official_source>"
    }
```
